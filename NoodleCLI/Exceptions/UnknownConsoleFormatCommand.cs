﻿using System;

namespace NoodleCLI.Exceptions
{
    /// <summary>
    /// An exception that's raised when the Console Format command is Unknown
    /// </summary>
    [Serializable]
    class UnknownConsoleFormatCommand : Exception
    {
        public UnknownConsoleFormatCommand(string messsage): base(messsage)
        {
        }

    }
}
