﻿namespace NoodleCLI.Enums
{
    /// <summary>
    /// The statement type
    /// </summary>
    enum StatementType
    {
        PLAIN_TEXT,
        CONSOLE_COMMANDS
    }
}
