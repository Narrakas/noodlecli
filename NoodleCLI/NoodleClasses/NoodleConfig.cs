﻿using Newtonsoft.Json.Linq;
using System;
using System.IO;

/// <summary>
/// NoodleCLI Namespace
/// </summary>
namespace NoodleCLI
{

    /// <summary>
    /// Manages, and parses the configuration file for NoodleCLI
    /// </summary>
    class NoodleConfig
    {
        /// <summary>
        /// The loaded configuration
        /// </summary>
        public JObject Configuration;

        /// <summary>
        /// Public Constructor
        /// </summary>
        public NoodleConfig()
        {
            if (File.Exists($"{AppDomain.CurrentDomain.BaseDirectory}{Path.DirectorySeparatorChar}noodlecli.config.json") == false)
            {
                // Set as null, methods will return default values instead of trying to use this object
                Configuration = null;
            }
            else
            {
                try
                {
                    Configuration = JObject.Parse(File.ReadAllText($"{AppDomain.CurrentDomain.BaseDirectory}{Path.DirectorySeparatorChar}noodlecli.config.json"));
                }
                catch(Exception exception)
                {
                    NoodleClasses.NoodleConsole.PrintError(exception, this.GetType());
                }
            }
        }

    }

}
