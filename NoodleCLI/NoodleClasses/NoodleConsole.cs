﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace NoodleCLI.NoodleClasses
{
    /// <summary>
    /// Manipulates the console
    /// </summary>
    class NoodleConsole
    {
        /// <summary>
        /// Outputs an error using an exception
        /// </summary>
        /// <param name="Exception"></param>
        /// <param name="Source"></param>
        public static void PrintError(Exception Exception, Type Source)
        {
            Console.ResetColor();
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine($"\nAn exception was thrown at NoodleCLI:\n\n    {Exception.Message}\n    {Exception.Source}\n    {Exception.StackTrace}\n");
            Console.WriteLine($"    Class Name: {Source.Namespace}.{Source.Name}");
            Console.ResetColor();
        }

        /// <summary>
        /// Executes a format command
        /// </summary>
        /// <param name="Command">The command to process</param>
        /// <param name="ThrowException">Throws an exception if the command is invalid</param>
        /// <returns>Returns true if the command was executed, returns false if the command wasn't executed</returns>
        public static bool ExecuteFormatCommand(string Command, bool ThrowException)
        {
            if (Command.ToUpper().Contains("CFC"))
            {
                var Value = 0;
                try
                {
                    Value = Convert.ToInt32(Regex.Match(Command, @"\(([^)]*)\)").Groups[1].Value);
                }
                catch
                {
                    return false;
                }
                switch (Value)
                {
                    case 0:
                        Console.ForegroundColor = ConsoleColor.Black;
                        return true;

                    case 1:
                        Console.ForegroundColor = ConsoleColor.Blue;
                        return true;

                    case 2:
                        Console.ForegroundColor = ConsoleColor.Cyan;
                        return true;

                    case 3:
                        Console.ForegroundColor = ConsoleColor.DarkBlue;
                        return true;

                    case 4:
                        Console.ForegroundColor = ConsoleColor.DarkCyan;
                        return true;

                    case 5:
                        Console.ForegroundColor = ConsoleColor.DarkGray;
                        return true;

                    case 6:
                        Console.ForegroundColor = ConsoleColor.DarkGreen;
                        return true;

                    case 7:
                        Console.ForegroundColor = ConsoleColor.DarkMagenta;
                        return true;

                    case 8:
                        Console.ForegroundColor = ConsoleColor.DarkRed;
                        return true;

                    case 9:
                        Console.ForegroundColor = ConsoleColor.DarkYellow;
                        return true;

                    case 10:
                        Console.ForegroundColor = ConsoleColor.Gray;
                        return true;

                    case 11:
                        Console.ForegroundColor = ConsoleColor.Green;
                        return true;

                    case 12:
                        Console.ForegroundColor = ConsoleColor.Magenta;
                        return true;

                    case 13:
                        Console.ForegroundColor = ConsoleColor.Red;
                        return true;

                    case 14:
                        Console.ForegroundColor = ConsoleColor.White;
                        return true;

                    case 15:
                        Console.ForegroundColor = ConsoleColor.Yellow;
                        return true;

                    default:

                        return false;
                }
            }
            else
            {
                if (ThrowException == true)
                {
                    throw new Exceptions.UnknownConsoleFormatCommand($"The command \"{Command}\" is an unknown console format command");
                }
                return false;
            }
            
        }
    }
}
