﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NoodleCLI
{
    /// <summary>
    /// Main Program for NoodleCLI
    /// </summary>
    class Program
    {

        /// <summary>
        /// Configuration for NoodleCLI
        /// </summary>
        static NoodleConfig Configuration;

        /// <summary>
        /// Main Execution Method
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
           
        }

        /// <summary>
        /// Updates the Title of the Console
        /// </summary>
        static void UpdateTitle()
        {
            Console.Title = $"NoodleCLI - [{Environment.UserName}@{Environment.UserDomainName}] {Directory.GetCurrentDirectory()}";
        }

        /// <summary>
        /// Prompts the user, and returns the input
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        static string Prompt(string text)
        {
            Console.Write(text);
            return(Console.ReadLine());
        }
    }
}
